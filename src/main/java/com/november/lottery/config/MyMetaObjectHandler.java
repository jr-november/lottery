package com.november.lottery.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.java.Log;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Log
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // @since 快照：3.0.7.2-SNAPSHOT， @since 正式版暂未发布3.0.7
        this.setInsertFieldValByName("createdTime", LocalDateTime.now(), metaObject);
        this.setInsertFieldValByName("updatedTime", LocalDateTime.now(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setInsertFieldValByName("updatedTime", LocalDateTime.now(), metaObject);
    }
}
