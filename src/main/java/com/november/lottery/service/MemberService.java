package com.november.lottery.service;

import com.november.lottery.base.BaseException;
import com.november.lottery.base.MyService;
import com.november.lottery.domain.Member;

import java.util.List;

/**
 * 平台活动上架接口
 * @author Administrator
 */
public interface MemberService extends MyService<Member> {
    Member checkExist(String openId, String classify);
    List<Member> getUnusedList(String classify);
    List<Member> listByClassify(String classify);
    Member join(String code, String classify) throws BaseException;

    String genCode(String classify, Integer sort) throws BaseException;

    Integer getNextSort(String classify);
}
