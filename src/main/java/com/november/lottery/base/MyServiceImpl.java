package com.november.lottery.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.TableInfoHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.november.lottery.utils.PageUtils;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Gider
 */
public class MyServiceImpl<M extends MyMapper<T>, T> extends ServiceImpl<M , T> implements MyService<T> {

    /**
     * 保存或更新后，失败抛出错误, 成功则返回对象
     */
    @Override
    public T saveOrUpdateAndReturn(T t) throws BaseException {
        boolean result = this.saveOrUpdate(t);
        if(result) {
            return t;
        }
        throw BaseException.throwDataError("保存失败");
    }

    /**
     * 使用Jpa的方式分页
     * @param pageable jpa的分页参数
     */
    @Override
    public IPage<T> pageViaJpa(Pageable pageable) {
        return page(PageUtils.toMybatisQuery(pageable));
    }

    /**
     * 使用Jpa的方式分页
     * @param pageable jpa的分页参数
     * @param queryWrapper 查询条件
     */
    @Override
    public IPage<T> pageViaJpa(Pageable pageable, Wrapper<T> queryWrapper) {
        return page(PageUtils.toMybatisQuery(pageable), queryWrapper);
    }

}
