package com.november.lottery.base;

import com.november.lottery.constant.HttpCustomStatusEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jr.November
 * 基础异常，用于全局异常处理
 * 前端总是以code为准，status为辅助判断
 */
@Setter
@Getter
public class BaseException extends Exception {
    /**
     * 自定义状态
     */
    private HttpCustomStatusEnum status;
    /**
     * 错误码
     */
    private int code;
    /**
     * 后台给的错误消息
     */
    private String message;

    /**
     * 通过状态码返回
     */
    public BaseException(HttpCustomStatusEnum status) {
        this.status = status;
        this.message = status.reasonPhrase;
        this.code = status.value;
    }

    /**
     * 通过状态码返回
     */
    public BaseException(HttpCustomStatusEnum status, String message) {
        this.status = status;
        this.message = message;
        this.code = status.value;
    }

    /**
     * 以code为准
     */
    public BaseException(int code, String message) {
        this.code = code;
        this.message = message;
        this.status = HttpCustomStatusEnum.CODE_DEFINE;
    }

    /**
     * 生成一个数据找不到的错误
     * @param message 自定义message
     * @return 数据找不到的Exception
     */
    public static BaseException throwDataNotFound(String message) {
        return new BaseException(HttpCustomStatusEnum.DATA_NOT_FOUND, message);
    }
    /**
     * 生成一个数据有误的错误
     * @param message 自定义message
     * @return 数据找不到的Exception
     */
    public static BaseException throwDataError(String message) {
        return new BaseException(HttpCustomStatusEnum.DATA_ERROR, message);
    }
    /**
     * 生成一个没有权限的错误
     * @param message 自定义message
     * @return 数据找不到的Exception
     */
    public static BaseException noPermission(String message) {
        return new BaseException(HttpCustomStatusEnum.NO_PERMISSION, message);
    }
    public static BaseException create(HttpCustomStatusEnum error) {
        return new BaseException(HttpCustomStatusEnum.DATA_ERROR);
    }
}
