package com.november.lottery.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Mybatis-plus base配置
 * @author Gider
 */
public interface MyMapper<T> extends BaseMapper<T> {
}
