package com.november.lottery.base;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface MyService<T> extends IService<T> {

    /**
     * 保存或更新后，失败抛出错误, 成功则返回对象
     */
    T saveOrUpdateAndReturn(T t) throws BaseException;

    IPage<T> pageViaJpa(Pageable pageable);

    IPage<T> pageViaJpa(Pageable pageable, Wrapper<T> queryWrapper);
}
