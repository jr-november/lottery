package com.november.lottery.domain;

import com.november.lottery.base.MyBaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * erp活动实体
 * @author Administrator
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member extends MyBaseModel {

    private String nickname;
    private String avatar;
    private String openId;
    private String result;
    private String country;
    private String province;
    private String city;
    private String classify;
    private Integer sort;
    private String number;
}
