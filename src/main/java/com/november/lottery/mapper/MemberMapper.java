package com.november.lottery.mapper;

import com.november.lottery.base.MyMapper;
import com.november.lottery.domain.Member;

/**
 * erp每日对账统计mapper
 * @author Administrator
 */
public interface MemberMapper extends MyMapper<Member> {
}
