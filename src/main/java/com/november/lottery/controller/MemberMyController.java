package com.november.lottery.controller;

import com.november.lottery.base.BaseException;
import com.november.lottery.base.BaseMyController;
import com.november.lottery.base.MyService;
import com.november.lottery.domain.Member;
import com.november.lottery.service.MemberService;
import com.november.lottery.utils.WebUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

/**
 * erp管理城市商圈
 *
 * @author Administrator
 */
@RestController
@RequestMapping("member")
@Log
public class MemberMyController extends BaseMyController<Member> {


    @Autowired
    private MemberService memberService;

    @Override
    public MyService<Member> getService() {
        return memberService;
    }

    /**
     * 现场报名
     *
     * @param code wxCode
     */
    @RequestMapping("joinTest")
    public ResponseEntity<Object> joinTest(String code, String classify) throws BaseException {

        String openId = code;
        // 从微信获取信息
        Member member = new Member();
        member.setNickname(code);
        member.setOpenId(openId);
        member.setAvatar(code);
        member.setCountry(code);
        member.setProvince(code);
        member.setCity(code);
        member.setClassify(classify);
        // 检测openid是否重复
        Member check = memberService.checkExist(openId, classify);
        if (check != null) {
            return WebUtils.success(check);
        }
        // 获取最大序号
        Integer sort = memberService.getNextSort(classify);
        String number = memberService.genCode(classify, sort);
        member.setSort(sort);
        member.setNumber(number);
        check = memberService.saveOrUpdateAndReturn(member);
        // 保存进数据库
        return WebUtils.success(check);

    }

    /**
     * 现场报名
     *
     * @param code wxCode
     * @param classify 抽奖批次
     */
    @RequestMapping("join")
    public ResponseEntity<Object> join(String code, String classify) throws BaseException {

        return WebUtils.success(memberService.join(classify, code));

    }

    /**
     * 现场报名
     *
     * @param classify 开奖批次
     */
    @RequestMapping("listAll")
    public ResponseEntity<Object> listAll(String classify) throws BaseException {

        // 获取全部名单
        List<Member> members = memberService.listByClassify(classify);
        return WebUtils.success(members);
    }

    /**
     * 现场报名
     *
     * @param classify 开奖批次
     */
    @RequestMapping("listOpen")
    public ResponseEntity<Object> listOpen(String classify) throws BaseException {

        // 获取全部名单
        List<Member> members = memberService.getUnusedList(classify);
        return WebUtils.success(members);
    }

    /**
     * 现场报名
     *
     * @param no 开奖项目
     */
    @RequestMapping("open")
    public ResponseEntity<Object> open(String no, String classify) throws BaseException {

        // 获取全部名单
        List<Member> canList = memberService.getUnusedList(classify);
        // 选择一个
        int size = canList.size();
        if(size==0) {
            throw BaseException.throwDataError("备选池已经空了");
        }
        Random random = new Random();
        // 获奖序号
        int index = random.nextInt(size);
        Member member = canList.get(index);
        member.setResult(no);
        // 保存进数据库,并且返回到前端
        memberService.saveOrUpdateAndReturn(member);
        return WebUtils.success(member);
    }
}
