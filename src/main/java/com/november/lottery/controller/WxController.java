package com.november.lottery.controller;

import com.november.lottery.base.BaseException;
import com.november.lottery.utils.WebUtils;
import com.november.lottery.wx.WxMpConfiguration;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @author Jr.November
 */
@RestController
@RequestMapping("wx")
public class WxController {
    @Autowired
    private WxMpConfiguration wxMpConfiguration;
    /**
     * 获取微信js-sdk授权
     */
    @GetMapping("getJsConfig")
    public ResponseEntity<Object> getJsConfig(String url) throws BaseException {

        WxMpService wxMpService = wxMpConfiguration.service();
        try {
            return WebUtils.success(wxMpService.createJsapiSignature(URLDecoder.decode(url, "UTF-8")));
        } catch (WxErrorException | UnsupportedEncodingException e) {
            throw BaseException.throwDataError("微信授权失败");
        }
    }
}
