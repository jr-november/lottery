package com.november.lottery.wx;

import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * wechat mp configuration
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class WxMpConfiguration {

    @Autowired
    private WxMpProperties properties;

    public WxMpService service() {
        WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
        // 设置微信公众号的appid
        config.setAppId(properties.getAppId());
        // 设置微信公众号的app corpSecret
        config.setSecret(properties.getSecret());
        // 设置微信公众号的token
        config.setToken(properties.getToken());
        // 设置微信公众号的EncodingAESKey
        config.setAesKey(properties.getAesKey());

        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(config);
        return service;
    }
}
