package com.november.lottery.wx;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * wechat mp properties
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Setter
@Getter
@Component
public class WxMpProperties {
    /**
     * 设置微信公众号的appid
     */
    @Value("${wx.mp.configs.appId}")
    private String appId;

    /**
     * 设置微信公众号的app secret
     */
    @Value("${wx.mp.configs.secret}")
    private String secret;

    /**
     * 设置微信公众号的token
     */
    @Value("${wx.mp.configs.token}")
    private String token;

    /**
     * 设置微信公众号的EncodingAESKey
     */
    @Value("${wx.mp.configs.aesKey}")
    private String aesKey;
}
