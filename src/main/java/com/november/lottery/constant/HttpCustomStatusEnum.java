package com.november.lottery.constant;

/**
 * @author Jr.November
 */
public enum HttpCustomStatusEnum {
    /**
     * 用户未登录
     */
    NOT_LOGIN(401, "用户未登录"),
    /**
     * 没有权限访问
     */
    NO_PERMISSION(403, "没有权限"),
    /**
     * 通过code给前端判定
     */
    CODE_DEFINE(600, "前端判定"),
    /**
     * 唯一性验证失败
     */
    ALREADY_USED(601, "唯一性验证失败"),
    /**
     * 数据格式有误
     */
    DATA_ERROR(602, "数据格式有误"),
    /**
     * 数据找不到
     */
    DATA_NOT_FOUND(604, "找不到所指代的数据"),
    /**
     * 未知错误
     */
    UNKNOWN_ERROR(666, "未知错误");

    public final int value;

    public final String reasonPhrase;

    HttpCustomStatusEnum(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }
}
