package com.november.lottery.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

/**
 * Spring Controller 通用方法
 * @author Gider
 */
public class WebUtils {
    /**
     * 返回成功
     */
    public static ResponseEntity<Object> success(Object obj){
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    /**
     * 返回成功消息
     * String 因为单独返回会造成JSON解析失败，前端会把String当json处理
     */
    public static ResponseEntity<Object> success(String message){
        return new ResponseEntity<>(Collections.singletonMap("message",message), HttpStatus.OK);
    }

}
