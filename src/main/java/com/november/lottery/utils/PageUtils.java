package com.november.lottery.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * 分页工具
 *
 * mybatis的分页从 1 开始，
 * jpa 分页从 0 开始
 * 暂时不支持条件转换
 * @author Gider
 *
 */
public class PageUtils {

    /**
     * JPA的分页查询转成Mybatis分页查询
     */
    public static <T> Page<T> toMybatisQuery(Pageable pageable) {
        int current = pageable.getPageNumber() + 1;
        int size = pageable.getPageSize();
        return new Page<>(current, size);
    }
}
